<footer>	
    <div class="frase">
		<div class="container">
			<div class="icono-frase">
				<img src="img/quote.png">
			</div>
			<div class="texto-frase parrafo">
				<p>Los obstaculos son lo que aparece cuando dejamos de mirar el objetivo.</p>
				<p>Henry Ford</p>
			</div>
		</div>
	</div>
	<div class="redesandcopyright">
		<div class="container">
			<div class="centra-texto">
				<span>
					Copyright © 2018
					<a href="http://www.sidev.com.ve/comelca">Comelca</a>
				</span>
			</div>
		</div>
	</div>
</footer>