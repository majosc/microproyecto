<section id="servicios">
	<div class="container">
		<h2>Nuestros servicios</h2>
		<p>Gestión de Redes (Telecom): las redes de telcomunicación se vuelven cada día más complejas, con plataformas híbridas donde coexisten diversas tecnologías y diversos proveedores, que imponen retos a la gestión diaria. Nuestro equipo se encarga de Ingeniería de Red, Implementación de Red, Gestión de cambios de Red. Somos proveedores de Internet para pequeñas y medianas empresas, asi como de Internet Residencial.</p><br>
		<p>Software Factory: nos enfocamos en el desarrollo de Software backend (Java, .NET, PHP y Python) y frontend (HTML5, CCS3, JavaScript), Desarrollo de APPs, Desarrollo de Bases de Datos en ORACLE PL/SQL, MariaDB o Postgre. Nos encargamos de desarrollos y mantenimientos a la medida, desarrollo de soluciones móviles y consultoría para la implementación de las mejores tecnologías necesarias para su modelo de negocio.</p>
	</div>
	<div class="container container-big">
		<div class="grid-item" style="background: url(img/it_desarrollador.jpg) center top no-repeat; background-size: cover;"></div>
		<div class="grid-item" style="background: url(img/it_sistemas.jpg) center top no-repeat; background-size: cover;"></div>
		<div class="grid-item" style="background: url(img/it_tecnologia.jpg) center top no-repeat; background-size: cover;"></div>
		<div class="grid-item" style="background: url(img/it_redes.jpg) center top no-repeat; background-size: cover;"></div>
	</div>
</section>