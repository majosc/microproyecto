<section id="clientes">
	<div class="container">
		<h2>Nuestros clientes, nuestra razón de ser</h2>
		<p>Con mas de 10 años de trayectoria, hoy manejamos una cartera de clientes que son nuestros socios comerciales, nuestra razón de ser y nuestro motivo para continuar a la vanguardia de la tecnología.</p>
	</div>
	<div class="container container-big">
		<div class="grid-item-customers" style="background: url(img/cargill.png) center top no-repeat; background-size: contain;"></div>
		<div class="grid-item-customers" style="background: url(img/empresas-polar.png) center top no-repeat; background-size: contain;"></div>
		<div class="grid-item-customers" style="background: url(img/praxair.png) center top no-repeat; background-size: contain;"></div>
		<div class="grid-item-customers" style="background: url(img/produsal.png) center top no-repeat; background-size: contain;"></div>
	</div>
</section>