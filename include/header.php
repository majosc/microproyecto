<header>
	<nav class="menu_contenedor">
		<div class="container container-big">
<!--			<div class="menu_boton"></div>-->
		    <ul class="menu">
				<li id="home-icon"><a href="#top" style="font-size: 20px"><i class="fa fa-home"></i></a></li>
				<li class="hide-item"><a href="#nosotros">NOSOTROS</a></li>
				<li class="hide-item"><a href="#servicios">SERVICIOS</a></li>
				<li class="hide-item"><a href="#clientes">NUESTROS CLIENTES</a></li>
				<li class="hide-item"><a href="#contacto">CONTACTO</a></li>
				
				<?php
					if(isset($_SESSION['usuario'])){
					    if($_SESSION['usuario']['rol']==='administrador'){
                            echo "<li><a href='admin.php'>". $_SESSION['usuario']['nombre'] ."</a></li>";
                        }
                        else{
                            echo "<li><a>". $_SESSION['usuario']['nombre'] ."</a></li>";
                        }

						echo "<li id='salir-icon'><a href='logout.php'><i class='fa fa-sign-out' aria-hidden='true'></i></a></li>";
					}else{
						echo "<li><a href='viewlogin.php'>ENTRAR</a></li>";
					}
				?>
								
			</ul>
		</div>
	</nav>
</header>