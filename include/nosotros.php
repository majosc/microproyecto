<section id="nosotros">
	<div class="container">
		<h2>Somos una empresa de servicios IT</h2>
		<p>Somos un equipo de profesionales altamente calificados que brindamos servicios en dos de las principales ramas de las tegnologias, desarrollo de aplicaciones para ambientes web, desktop y movil y servicos de telecomunicaciones, redes de área local (LAN), le brindamos soluciones de acceso a internet con nuestra red Ubiquity, una red confiable y estable que le permitirá mantenerser conectado en todo momento, todos nuestros servicios están soportados por la excelente calidad de nuestra infraestructura y nuestra vocación de servicios.</p>
		<p>Nuestros profesionales están certificados en desarrollo de aplicaciones web con estándares HTML5 y CSS3, nuestro equipo de frontend es especialista en JavaScript, Jquery y Angular. Nuestro equipo de backend son especialistas en php y python, así como manejo de bases de datos transaccionales como MariaDB, Postgresql y Oracle.</p>
		<p>También contamos con certificaciones Cisco, Mikrotik y Ubiquity para brindarles los mejores resultados en sus implementaciones de infraestructuras de Red, tanto cableadas como inalámbrica, trabajamos con equipos certificados para brindarles la mejor experiencia, esa es nuestra meta como equipo de soluciones IT.</p>
	</div>
	<div class="panorama"></div>
</section>