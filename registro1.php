<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Registro de usuarios</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <link rel="icon" href="img/faviconMicro/favicon.ico" type="image/x-icon" />
    <link href="css/estilos.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-3.3.1.js" type="text/javascript"></script>		<!--Version de jquery para entorno de desarrollo-->
    <script src="js/funciones.js" type="text/javascript"></script>
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:400,700" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="apple-touch-icon" sizes="180x180" href="img/faviconMicro/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/faviconMicro/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/faviconMicro/favicon-16x16.png">
    <link rel="manifest" href="img/faviconMicro/site.webmanifest">
    <link rel="mask-icon" href="img/faviconMicro/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

</head>
<body>

<div class="main-container">
    <div class="contenido">
        <div class="cont-general">
            <div class="ancho">
                <div class="saludo f02 ">
                    <div class="form-container">
                        <div class="f02 title">
                            <h1>REGISTRO</h1>
                        </div>
                        <form method="post" action="registro.php" enctype="multipart/form-data">
                            <div class="login-form">
                                <!--<input name="password" type="password" placeholder="contraseña">-->
                                <input required name="nombre" type="text" placeholder="Nombre">
                                <input required name="apellido" type="text" placeholder="Apellido">
                                <!--<input name="cedula" type="number" placeholder="Cédula">-->
                                <input required name="correo" type="email" placeholder="Correo">
                                <input required name="password" type="password" placeholder="Contraseña">
                            </div>
                            <div class="file-input">
                                <span>Sube una imagen de perfil</span>
                                <input type="hidden" name="MAX_FILE_SIZE" value="25000000">
                                <input name="foto" style="display: block" type="file">

                            </div>
                            <div class="opc-form f01">
                                <button type="submit" class="btn btn-login">Registrate</button>
                                <!--<a href="registro.html"><h3>Registrate</h3></a>-->
                            </div>
                            <a class="cb a-login" href="viewlogin.php">Iniciar Sesión</a>
                        </form>
                        <?php
                        if(isset($_GET["codigo"])){
                            switch ($_GET["codigo"]){
                                case 1: echo "<p class='msg error'>Error: El usuario ya se encuentra registrado</p>"; break;
                                case 2: echo "<p class='msg success'>Usuario registrado exitosamente</p>"; break;
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



</body>
</html>