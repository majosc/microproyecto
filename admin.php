<!DOCTYPE html>
<html lang="es">
<head>
    <title>Comelca</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <link rel="icon" href="img/faviconMicro/favicon.ico" type="image/x-icon" />
    <link href="css/estilos.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-3.3.1.js" type="text/javascript"></script>		<!--Version de jquery para entorno de desarrollo-->
    <script src="js/funciones.js" type="text/javascript"></script>
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:400,700" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="apple-touch-icon" sizes="180x180" href="img/faviconMicro/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/faviconMicro/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/faviconMicro/favicon-16x16.png">
    <link rel="manifest" href="img/faviconMicro/site.webmanifest">
    <link rel="mask-icon" href="img/faviconMicro/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
</head>
<body>
<?php
session_start();
$partes = array(0 => "header.php");

    foreach ($partes as $parte) {
        include (__DIR__ . '/include/' . $parte);
    }

?>

<div class="main-container bgb">
    <div class="contenido">
        <!--        <div class="fotof imgb" style="background-image: url('img/tecnologia-en-constante-crecimiento.jpg')">
                    <div class="sombra pa"></div>
                </div>-->
        <div class="cont-general">
            <div class="ancho">
                <div class="saludo f02 fs">
                    <div class="admin-container">
                        <div class="f02 title-black">
                            <h1>Usuarios registrados</h1>
                        </div>

                        <table>
                            <tr>
                                <th>Nombre</th>
                                <th>Apellido</th>
                                <th>Correo</th>
                                <th>Rol</th>
                            </tr>

                                <?php

                                    foreach ($_SESSION['usuarios']  as $usuario){
                                       echo " <tr>";
                                        echo "<td>".$usuario['nombre']."</td>";
                                        echo "<td>".$usuario['apellido']."</td>";
                                        echo "<td>".$usuario['email']."</td>";
                                        echo "<td>".$usuario['rol']."</td>";
                                        echo "</tr>";
                                    }
                                ?>
                        </table>

                        <!--<form method="post" action="login.php">
                            <div class="login-form">
                                <input name="correo" type="email" placeholder="correo">
                                <input name="password" type="password" placeholder="contraseña">
                            </div>
                            <div class="opc-form">
                                <button class="btn btn-login">Ingresar</button>
                                <a href="registro1.php"><h3>Registrate</h3></a>
                            </div>
                        </form>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



</body>
</html>